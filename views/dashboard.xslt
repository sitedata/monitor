<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
// Webserver template
//
//-->
<xsl:template match="webserver">
<div class="panel panel-default webserver">
<div class="panel-heading {status}" onClick="javascript:$('div#webserver{@id}').slideToggle()"><xsl:value-of select="name" /></div>
<div id="webserver{@id}" class="panel-body"><table>
<xsl:if test="version!=''">
<tr><td>Version:</td><td><xsl:value-of select="version" />
<xsl:if test="modules">
<span class="dropdown">
<button class="btn btn-default btn-xs dropdown-toggle modules" type="button" data-toggle="dropdown">Modules <span class="caret"></span></button><ul class="dropdown-menu">
<xsl:for-each select="modules/module">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ul></span>
</xsl:if>
</td></tr>
<tr><td>Up to date:</td><td><xsl:if test="uptodate='no'"><xsl:attribute name="class">warning</xsl:attribute></xsl:if><xsl:value-of select="uptodate" /></td></tr>
</xsl:if>
<tr><td>Active sync:</td><td><xsl:value-of select="active" /></td></tr>
<tr><td>Sync address:</td><td><xsl:value-of select="address" /></td></tr>
<tr><td>Latest sync fails:</td><td><xsl:if test="errors>0"><xsl:attribute name="class">warning</xsl:attribute></xsl:if><xsl:value-of select="errors" /></td></tr>
</table></div>
</div>
</xsl:template>

<!--
//
//  Weblogs template
//
//-->
<xsl:template match="weblogs">
<div class="weblogs">
<h2>Hiawatha weblog</h2>
<xsl:for-each select="weblog">
<div class="panel panel-default weblog">
<div class="panel-heading {status}" onClick="javascript:$('div#weblog{@id}').slideToggle()"><xsl:value-of select="title" /><span class="timestamp"><xsl:value-of select="timestamp" /></span></div>
<div id="weblog{@id}" class="panel-body"><xsl:value-of disable-output-escaping="yes" select="content" />
<div class="readmore"><a href="https://www.hiawatha-webserver.org/weblog/{@id}">Show weblog</a></div></div>
</div>
</xsl:for-each>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Dashboard</h1>
<div class="row">
<div class="col-sm-6">
<h2>Monitored webservers</h2>
<xsl:apply-templates select="webserver" />
</div>

<div class="col-sm-6">
<h2>Alerts for today<span>[<a href="javascript:return false" id="opener">?</a>]</span></h2>
<div id="dialog" title="Percentages">
<p>The shown change vales represent the surplus value for the current day, compared to the median for the previous days.</p>
<p>So, when the median for the previous days is 200 and the value for the current day is 900, the change is 3.5x. Because (900-200)/200 = 3.5.</p>
<p>The current time is taken into account when calculating the median for the previous days. Only count values starting at <xsl:value-of select="threshold_value" /> and change values starting at <xsl:value-of select="threshold_change" /> are shown. The alerts are refreshed every <xsl:if test="page_refresh>1"><xsl:value-of select="page_refresh" /></xsl:if> minute<xsl:if test="page_refresh>1">s</xsl:if>.</p>
</div>
<div class="alerts" refresh="{page_refresh}"></div>
</div>
</div>

<xsl:apply-templates select="weblogs" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
