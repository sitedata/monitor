<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="graph.xslt" />

<!--
//
//  Day template
//
//-->
<xsl:template match="day">
<div class="browse_day btn-group" style="top:-{../graph/@height+132}px">
<a href="/{/output/page}/{@type}/{@yesterday}" class="btn btn-default btn-xs">&lt;</a>
<a href="/{/output/page}/{@type}/{@tomorrow}" class="btn btn-default btn-xs">&gt;</a>
</div>

<table class="table table-striped table-condensed table-xs">
<thead class="table-xs">
<tr>
<xsl:if test="@hostnames='yes'">
<th class="hostname">Hostname</th>
</xsl:if>
<th class="webserver">Webserver</th>
<th class="count"><xsl:value-of select="@label" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="stat">
<tr>
<xsl:if test="../@hostnames='yes'">
<td><span class="table-xs">Hostname:</span><xsl:value-of select="hostname" /></td>
</xsl:if>
<td><span class="table-xs">Webserver:</span><xsl:value-of select="webserver" /></td>
<td><span class="table-xs">Count:</span><xsl:value-of select="count" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<a href="/{/output/page}" class="btn btn-default">Back</a>
</xsl:template>

</xsl:stylesheet>
